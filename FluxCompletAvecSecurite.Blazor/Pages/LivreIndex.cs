﻿using Blazored.Modal;
using Blazored.Modal.Services;
using FluxCompletAvecSecurite.Blazor.Models;
using FluxCompletAvecSecurite.Blazor.Services;
using FluxCompletAvecSecurite.Blazor.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Blazor.Pages
{
    public partial class LivreIndex
    {
        [Inject]
        public LivreService Service { get; set; }

        [Inject]
        public IJSRuntime jSRuntime { get; set; }

        [Inject]
        public IModalService ModalService { get; set; }

        public IEnumerable<LivreModel> Model { get; set; }

        protected async override Task OnInitializedAsync()
        {
            Model = await Service.GetAsync();
        }

        public async Task DeleteAsync(int id)
        {
            try
            {
                IModalReference modal = ModalService.Show<ConfirmBox>();
                ModalResult result = await modal.Result;
                if (!result.Cancelled)
                {
                    await Service.DeleteAsync(id);
                    await jSRuntime.InvokeVoidAsync("success", "Suppression ok");
                    Model = await Service.GetAsync();
                }
            }
            catch(Exception e)
            {
                await jSRuntime.InvokeVoidAsync("error", e.Message);
            }
        }
    }
}
