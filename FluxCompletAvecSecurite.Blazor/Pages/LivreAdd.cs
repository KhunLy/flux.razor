﻿using FluxCompletAvecSecurite.Blazor.Models;
using FluxCompletAvecSecurite.Blazor.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Blazor.Pages
{
    public partial class LivreAdd
    {
        public LivreFormModel Form { get; set; }

        [Inject]
        public LivreService Service { get; set; }

        [Inject]
        public IJSRuntime jsRuntime { get; set; }

        [Inject]
        public NavigationManager Nav { get; set; }

        protected override void OnInitialized()
        {
            Form = new LivreFormModel();
        }

        public async Task SubmitAsync()
        {
            try
            {
                await Service.AddAsync(Form);
                await jsRuntime.InvokeVoidAsync("success", "OK", "Insertion Ok");
                Nav.NavigateTo("/livre");
            }
            catch(Exception e)
            {
                await jsRuntime.InvokeVoidAsync("error", "KO", "server error");
            }
        }
    }
}
