﻿using FluxCompletAvecSecurite.Blazor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Blazor.Services
{
    public class LivreService
    {

        private HttpClient http;

        public LivreService(HttpClient http)
        {
            this.http = http;
        }

        public async Task AddAsync(LivreFormModel form)
        {
            await http.PostAsJsonAsync("livre", form);
        }

        public async Task<IEnumerable<LivreModel>> GetAsync()
        {
            return await http.GetFromJsonAsync<IEnumerable<LivreModel>>("livre");
        }

        public async Task DeleteAsync(int id)
        {
            await http.DeleteAsync("livre/" + id);
        } 
    }
}
