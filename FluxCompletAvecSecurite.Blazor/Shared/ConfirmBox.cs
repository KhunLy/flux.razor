﻿using Blazored.Modal;
using Blazored.Modal.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FluxCompletAvecSecurite.Blazor.Shared
{
    public partial class ConfirmBox
    {

        [CascadingParameter] public BlazoredModalInstance Modal { get; set; }

        public void Yes() 
        {
            Modal.Close();
        }

        public void No()
        {
            Modal.Cancel();
        }
    }
}
